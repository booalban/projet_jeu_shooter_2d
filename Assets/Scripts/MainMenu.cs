using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void Menu_Start(string level)
    {
        SceneManager.LoadScene(level);
    }

    public void Menu_Quit()
    {
        Application.Quit();
        Debug.Log("quit");
    }
}
