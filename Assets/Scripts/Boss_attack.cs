using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss_attack : MonoBehaviour
{
    [SerializeField] private float seconds;
    [SerializeField] private string type;
    private float upLeftCamera;
    private float bottomRightCamera;
    private float siz;
    private float siz_player;

    private GameObject health_bar;

    private float timer;
    private float position_y;
    Quaternion rotation = Quaternion.Euler(0, 0, 180);
    // Start is called before the first frame update
    void Start()
    {
        if (type == "Overlord")
        {
            timer = 3f;
        }
        else
        {
            timer = seconds;
        }

        health_bar = GameObject.FindGameObjectWithTag("Health_bar");
        upLeftCamera = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0)).y;
        bottomRightCamera = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0)).y;
        siz_player = GameObject.FindGameObjectWithTag("Player").GetComponent<SpriteRenderer>().bounds.size.y;
        GameObject shot = (GameObject)Instantiate(Resources.Load("ShotEnemy"), Vector3.zero, rotation);
        siz = shot.GetComponent<SpriteRenderer>().bounds.size.y;
        Destroy(shot);
        position_y = upLeftCamera - siz * 2;

    }

    // Update is called once per frame
    void Update()
    {
        if (type == "Barrier")
        {
            if (timer <= 0)
            {
                if (!Pause_Menu.Paused)
                {
                    bool exit = false;
                    while (position_y >= bottomRightCamera)
                    {
                        Vector3 pos = new Vector3(gameObject.transform.position.x - (gameObject.GetComponent<SpriteRenderer>().bounds.size.x / 2),
                            position_y,
                            gameObject.transform.position.z);
                        GameObject shot = (GameObject)Instantiate(Resources.Load("ShotEnemy"), pos, rotation);
                        shot.GetComponent<Enemies_Hp>().hp = 100;
                        shot.GetComponent<EnemiesShotEffect>().seconds = 3;
                        if ((exit == false && Random.value <= 0.02f) || (exit == false && position_y < bottomRightCamera + siz_player * 3))
                        {
                            position_y -= siz_player * 3;
                            exit = true;
                        }

                        position_y -= siz * 2;
                    }
                    exit = false;
                    position_y = upLeftCamera;

                }
                timer = seconds;
            }
            else
            {
                timer -= Time.deltaTime;
            }
        }
        else if (type == "Follow")
        {
            float range = Random.Range(0f, 10000f);
            float value_range = Random.Range(0, range);

            if (value_range < 25)
            {
                Shoot();
            }

            else if (value_range < 50)
            {
                Shoot2();
            }
        }else if(type == "Overlord")
        {
            if (timer <= 0)
            {
                Shoot_overlord();
                timer = 1f;
            }
            else
            {
                timer -= Time.deltaTime;
            }
        }

    }


    public void Shoot()
    {
        if (!Pause_Menu.Paused)
        {

            Vector3 pos1 = new Vector3(gameObject.transform.position.x - (gameObject.GetComponent<SpriteRenderer>().bounds.size.x / 2),
                           gameObject.transform.position.y + (gameObject.GetComponent<SpriteRenderer>().bounds.size.y / 4),
                           gameObject.transform.position.z);
            Vector3 pos2 = new Vector3(gameObject.transform.position.x - (gameObject.GetComponent<SpriteRenderer>().bounds.size.x / 2),
                gameObject.transform.position.y - (gameObject.GetComponent<SpriteRenderer>().bounds.size.y / 4),
                gameObject.transform.position.z);
            Quaternion rotation = Quaternion.Euler(0, 0, 0);
            Instantiate(Resources.Load("ShotTest"), pos1, rotation);
            Instantiate(Resources.Load("ShotTest"), pos2, rotation);
        }

    }



    public void Shoot2()
    {
        if (!Pause_Menu.Paused)
        {

            Vector3 pos1 = new Vector3(gameObject.transform.position.x - (gameObject.GetComponent<SpriteRenderer>().bounds.size.x / 1.75f),
                           gameObject.transform.position.y,
                           gameObject.transform.position.z);
            Quaternion rotation = Quaternion.Euler(0, 0, 180);
            Instantiate(Resources.Load("ShotSalve"), pos1, rotation);
            Instantiate(Resources.Load("ShotSalve"), pos1, rotation);
            Instantiate(Resources.Load("ShotSalve"), pos1, rotation);
            Instantiate(Resources.Load("ShotSalve"), pos1, rotation);
            Instantiate(Resources.Load("ShotSalve"), pos1, rotation);
            Instantiate(Resources.Load("ShotSalve"), pos1, rotation);
        }
    }

    public void Shoot_overlord()
    {
        if (!Pause_Menu.Paused)
        {
            float range = Random.Range(5f, 15f);

            Vector3 pos = new Vector3(gameObject.transform.position.x - gameObject.GetComponent<SpriteRenderer>().bounds.size.y / 2,
                gameObject.transform.position.y,
                gameObject.transform.position.z);
            Quaternion rotation = Quaternion.Euler(0, 0, 180);
            Instantiate(Resources.Load("doubleshot"), pos, rotation);


            Vector3 pos1 = new Vector3(gameObject.transform.position.x - gameObject.GetComponent<SpriteRenderer>().bounds.size.y / 2,
                gameObject.transform.position.y + gameObject.transform.position.y * range,
                gameObject.transform.position.z);
            Quaternion rotation1 = Quaternion.Euler(0, 0, 180);
            Instantiate(Resources.Load("doubleshot"), pos1, rotation1);


            Vector3 pos2 = new Vector3(gameObject.transform.position.x - gameObject.GetComponent<SpriteRenderer>().bounds.size.y / 2,
                gameObject.transform.position.y - gameObject.transform.position.y * range,
                gameObject.transform.position.z);
            Quaternion rotation2 = Quaternion.Euler(0, 0, 180);
            Instantiate(Resources.Load("doubleshot"), pos2, rotation2);
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Vector2 health_lvl = health_bar.GetComponent<RectTransform>().sizeDelta;
            health_bar.GetComponent<RectTransform>().sizeDelta = new Vector2(0, health_lvl.y);
        }
    }
}
