using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elec_Effect : MonoBehaviour
{
    private float timer = 3f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (timer <= 0)
        {
            Destroy(gameObject.GetComponent<Elec_Effect>());
        }
        timer -= Time.deltaTime;
        GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);

    }
}
