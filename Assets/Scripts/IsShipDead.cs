using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsShipDead : MonoBehaviour
{
    [SerializeField] private GameObject gameover;

    private Animator animator;
    private GameObject health_bar;
    private Vector2 health_lvl;
    private float timer = 1;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        health_bar = GameObject.FindGameObjectWithTag("Health_bar");
    }

    // Update is called once per frame
    void Update()
    {
        if (animator.GetBool("Trigger"))
        {
            if (timer <= 0)
            {
                gameover.SetActive(true);
                Time.timeScale = 0;
                Pause_Menu.Paused = true;
                Destroy(gameObject);
            }
            timer -= Time.deltaTime;
        }
        else
        {
            health_lvl = health_bar.GetComponent<RectTransform>().sizeDelta;
            if (health_lvl.x <= 0)
            {
                animator.SetBool("Trigger", true);
                GetComponent<AudioSource>().Play();
            }
        }

    }
}
