using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesShoot : MonoBehaviour
{
    [SerializeField] private float seconds;
    [SerializeField] private string type;
    private float timer;
    private int all_type = 0;
    // Start is called before the first frame update
    void Start()
    {
        timer = seconds;
    }

    // Update is called once per frame
    void Update()
    {
        if (timer <= 0)
        {
            Shoot();
            timer = seconds;
        }
        else
        {
            timer -= Time.deltaTime;
        }

    }

    public void Shoot()
    {
        if (!Pause_Menu.Paused)
        {
            Vector3 pos = new Vector3(gameObject.transform.position.x - gameObject.GetComponent<SpriteRenderer>().bounds.size.x,
                gameObject.transform.position.y,
                gameObject.transform.position.z);
            Quaternion rotation = Quaternion.Euler(0, 0, 180);
            if (type == "Default")
            {
                Instantiate(Resources.Load("ShotEnemy"), pos, rotation);
            }
            else if (type == "Elec")
            {
                Instantiate(Resources.Load("ShotEnemy_Elec"), pos, rotation);
            }
            else if (type == "Nova")
            {
                Instantiate(Resources.Load("ShotEnemy_Nova"), pos, rotation);
            }
            else if (type == "Laser")
            {
                Instantiate(Resources.Load("ShotEnemy_Laser"), pos, rotation);
            }
            else if (type == "All")
            {
                switch (all_type)
                {
                    case 0:
                        Instantiate(Resources.Load("ShotEnemy"), pos, rotation);
                        break;
                    case 1:
                        Instantiate(Resources.Load("ShotEnemy_Elec"), pos, rotation);
                        break;
                    case 2:
                        Instantiate(Resources.Load("ShotEnemy_Laser"), pos, rotation);
                        all_type = -1;
                        break;
                }
                all_type += 1;

            }
        }
    }
}
