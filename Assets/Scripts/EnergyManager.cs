using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyManager : MonoBehaviour
{
    [SerializeField] private float recharge_time;
    private float timer;
    private Vector2 energy_lvl;

    // Start is called before the first frame update
    void Start()
    {
        timer = recharge_time;
        energy_lvl = GetComponent<RectTransform>().sizeDelta;

    }

    // Update is called once per frame
    void Update()
    {
        energy_lvl = GetComponent<RectTransform>().sizeDelta;
        if (energy_lvl.x < 1118)
        {
            if (timer <= 0)
            {
                GetComponent<RectTransform>().sizeDelta = new Vector2(energy_lvl.x + 43, energy_lvl.y);
                timer = recharge_time;

            }
            timer -= Time.deltaTime;
        }
    }
}
