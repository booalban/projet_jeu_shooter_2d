using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class Boss2Moves : MonoBehaviour
{

    private float timer;



    void Start()
    {
        timer = 5f;
    }
    public float Loop()
    {
        if (timer < 2.5)
        {

            return (float)(-0.002);
        }


        return (float)(0.002);


    }

    // Update is called once per frame
    void Update()
    {
        if (!Pause_Menu.Paused)
        {
            float y = Loop();
            transform.position += new Vector3(0, y, 0);
            timer -= Time.deltaTime;
            if (timer < 0)
            {
                timer = 5f;
            }
        }
    }



    


}
