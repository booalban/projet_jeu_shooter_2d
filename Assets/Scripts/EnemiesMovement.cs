using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesMovement : MonoBehaviour
{
    [SerializeField] private Vector2 speed;
    private Vector3 leftCameraBorder;
    private Vector3 rightTopCameraBorder;
    private Vector2 siz;
    private GameObject health_bar;

    [SerializeField] private string type;
    // Start is called before the first frame update
    void Start()
    {
        leftCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        rightTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
        health_bar = GameObject.FindGameObjectWithTag("Health_bar");
        siz.y = GetComponent<SpriteRenderer>().bounds.size.y * transform.localScale.y;

        GetComponent<Rigidbody2D>().velocity = speed;

    }

    // Update is called once per frame
    void Update()
    {
        if(type == "Wave")
            GetComponent<Rigidbody2D>().velocity = new Vector2(speed.x, Mathf.Sin(transform.position.x) * speed.y);
        else if(type == "Diagonale")
        {
            if (transform.position.y - (siz.y / 2) < leftCameraBorder.y || transform.position.y + (siz.y / 2) > rightTopCameraBorder.y)
            {
                speed = new Vector2(speed.x, speed.y * -1);
                GetComponent<Rigidbody2D>().velocity = speed;
            }
        }
        else if(type == "Semi_Circle")
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(speed.x + Mathf.Cos(Time.time) * speed.x, Mathf.Sin(Time.time) * speed.y);
        }
        else if (type == "Circle")
        {
            if (Mathf.Cos(Time.time) * speed.x <= speed.x / 2)
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.Cos(Time.time) * (speed.x * 2), Mathf.Sin(Time.time) * speed.y);
            }
            else
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.Cos(Time.time) * speed.x, Mathf.Sin(Time.time) * speed.y);
            }
        }


        if (transform.position.x < leftCameraBorder.x)
            Destroy(gameObject);
        else if (transform.position.y - (siz.y / 2) < leftCameraBorder.y)
            transform.position = new Vector2(transform.position.x, leftCameraBorder.y + (siz.y / 2));
        else if (transform.position.y + (siz.y / 2) > rightTopCameraBorder.y)
            transform.position = new Vector2(transform.position.x, rightTopCameraBorder.y - (siz.y / 2));
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            speed = new Vector2(0, 0);
            Vector2 health_lvl = health_bar.GetComponent<RectTransform>().sizeDelta;
            if (health_lvl.x > 0)
                health_bar.GetComponent<RectTransform>().sizeDelta = new Vector2(health_lvl.x - 280, health_lvl.y);
            GetComponent<Enemies_Hp>().hp = 0;
        }
    }
}
