using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Score : MonoBehaviour
{
    public static int score_val;
    private TextMeshProUGUI score_text;
    // Start is called before the first frame update
    void Start()
    {
        score_text = GetComponent<TextMeshProUGUI>();
        
    }

    // Update is called once per frame
    void Update()
    {
        score_text.text = score_val.ToString();
    }
}
