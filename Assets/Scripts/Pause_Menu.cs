using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pause_Menu : MonoBehaviour
{
    public static bool Paused = false;
    [SerializeField] private string exit_level;
    [SerializeField] private AudioSource music;

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Pause()
    {
        Time.timeScale = 0;
        music.Pause();
        Paused = true;
    }

    public void Resume()
    {
        Time.timeScale = 1;
        music.Play();
        Paused = false;
    }

    public void Level_Quit()
    {
        Time.timeScale = 1;
        Paused = false;
        save();
        GameCreator.boss_spawn = false;
        GameCreator.trigger = false;
        Score.score_val = 0;
        SceneManager.LoadScene(exit_level);
    }

    public void Restart()
    {
        Time.timeScale = 1;
        Paused = false;
        save();
        GameCreator.boss_spawn = false;
        GameCreator.trigger = false;
        Score.score_val = 0;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void save()
    {
        if (GameObject.FindGameObjectWithTag("Victory") != null || (GameObject.FindGameObjectWithTag("GameController").GetComponent<GameCreator>().loop && GameObject.FindGameObjectWithTag("Player") == null))
        {
            string name = SceneManager.GetActiveScene().name;
            int score = Score.score_val;

            if (PlayerPrefs.GetInt(name) < score)
                PlayerPrefs.SetInt(name, score);
        }
    }
}
