using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss_Spawn : MonoBehaviour
{
    private Vector3 bottomRightCamera;
    private Vector2 siz;

    // Start is called before the first frame update
    void Start()
    {
        siz.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        siz.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;
        bottomRightCamera = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
        GetComponent<Rigidbody2D>().velocity = new Vector2(-3, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x + (siz.x / 2) <= bottomRightCamera.x)
        {
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            GetComponent<Animator>().SetTrigger("Spawn");
            foreach (BoxCollider2D i in GetComponents<BoxCollider2D>())
                i.enabled = true;
            GetComponent<Boss_attack>().enabled = true;
            Destroy(GetComponent<Boss_Spawn>());

        }

    }
}
