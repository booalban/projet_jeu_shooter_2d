using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipShoot : MonoBehaviour
{
    private GameObject energy;
    // Start is called before the first frame update
    void Start()
    {
        energy = GameObject.FindGameObjectWithTag("Energy_bar");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Shoot(string type)
    {
        Vector2 energy_lvl = energy.GetComponent<RectTransform>().sizeDelta;
        if (!Pause_Menu.Paused)
        {
            if (type == "Default" && energy_lvl.x >= 43)
            {
                energy.GetComponent<RectTransform>().sizeDelta = new Vector2(energy_lvl.x - 43, energy_lvl.y);
                Vector3 pos = new Vector3(gameObject.transform.position.x + gameObject.GetComponent<SpriteRenderer>().bounds.size.x,
                    gameObject.transform.position.y,
                    gameObject.transform.position.z);
                Instantiate(Resources.Load("Shot"), pos, gameObject.transform.rotation);

            }
            else if(type == "Special" && energy_lvl.x >= 172)
            {
                energy.GetComponent<RectTransform>().sizeDelta = new Vector2(energy_lvl.x - 172, energy_lvl.y);
                Vector3 pos = new Vector3(gameObject.transform.position.x + gameObject.GetComponent<SpriteRenderer>().bounds.size.x,
                    gameObject.transform.position.y,
                    gameObject.transform.position.z);
                Instantiate(Resources.Load("Special_Shot"), pos, gameObject.transform.rotation);
            }
        }
    }
}
