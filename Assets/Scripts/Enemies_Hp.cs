using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemies_Hp : MonoBehaviour
{
    public int hp;
    [SerializeField] private int score;
    private int hp_send;
    private Animator animator;


    private GameObject health_bar_player;

    private float timer = 0.8f;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        hp_send = hp;
        health_bar_player = GameObject.FindGameObjectWithTag("Health_bar");


    }

    // Update is called once per frame
    void Update()
    {

        if (animator.GetBool("Trigger"))
        {
            if (timer <= 0)
            {
                if (gameObject.tag == "Boss")
                {
                    if (GameObject.FindGameObjectWithTag("GameController").GetComponent<GameCreator>().loop == false)
                    {
                        GameCreator.trigger = true;
                    }
                    else
                    {
                        GameCreator.boss_spawn = false;
                        float rand = Random.value;
                        if (rand <= 0.33)
                        {
                            Instantiate(Resources.Load("Boss1"), new Vector3(18, 0, 0), Quaternion.Euler(0, 0, 90));
                        }
                        else if (rand <= 0.66)
                        {
                            Instantiate(Resources.Load("Duplicator"), new Vector3(18, 0, 0), Quaternion.Euler(0, 0, 90));
                        }
                        else
                        {
                            Instantiate(Resources.Load("Overlord"), new Vector3(18, 0, 0), Quaternion.Euler(0, 0, 90));
                        }
                    }
                }
                Destroy(gameObject);

            }
            timer -= Time.deltaTime;
        }

        else if (hp <= 0)
        {
            foreach (BoxCollider2D i in GetComponents<BoxCollider2D>())
                i.enabled = false;
            animator.SetBool("Trigger", true);
            GetComponent<AudioSource>().Play();

            Score.score_val += score;

            Vector2 hp_player = health_bar_player.GetComponent<RectTransform>().sizeDelta;
            if (hp_send == 3)
            {
                if (hp_player.x + 43 < 1118)
                    health_bar_player.GetComponent<RectTransform>().sizeDelta = new Vector2(hp_player.x + 21.5f, hp_player.y);
            }
            else if (hp_send <= 6 && hp_send > 3)
            {
                if (hp_player.x + 86 < 1118)
                    health_bar_player.GetComponent<RectTransform>().sizeDelta = new Vector2(hp_player.x + 43, hp_player.y);
                else
                    health_bar_player.GetComponent<RectTransform>().sizeDelta = new Vector2(1118, hp_player.y);
            }
            else if (hp_send <= 8 && hp_send > 6)
            {
                if (hp_player.x + 129 < 1118)
                    health_bar_player.GetComponent<RectTransform>().sizeDelta = new Vector2(hp_player.x + 86, hp_player.y);
                else
                    health_bar_player.GetComponent<RectTransform>().sizeDelta = new Vector2(1118, hp_player.y);
            }else if (hp_send >= 50 && gameObject.tag == "Boss")
            {
                if (hp_player.x + 559 < 1118)
                    health_bar_player.GetComponent<RectTransform>().sizeDelta = new Vector2(hp_player.x + 559, hp_player.y);
                else
                    health_bar_player.GetComponent<RectTransform>().sizeDelta = new Vector2(1118, hp_player.y);
            }
        }
    }
}
