using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCreator : MonoBehaviour
{
    public static bool trigger = false;
    public bool loop;
    public static bool boss_spawn = false;

    [SerializeField] private GameObject victory_screen;
    [SerializeField] private float gameTime;
    [SerializeField] private int enemies_number;
    [SerializeField] private int difficulty;
    private float enemy_intervale;
    private float timer;

    private float loop_gametime;

    
    private Vector3 siz;
    private Vector3 rightBottomCameraBorder;
    private Vector3 rightTopCameraBorder;

    // Start is called before the first frame update
    void Start()
    {
        rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
        rightTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
        enemy_intervale = gameTime / enemies_number;
        timer = enemy_intervale;
        loop_gametime = gameTime;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (trigger && !loop)
        {
            victory_screen.SetActive(true);
            Time.timeScale = 0;
            Pause_Menu.Paused = true;
            trigger = false;
        }

        if (timer <= 0 && gameTime >= 15 && !boss_spawn)
        {
            float num = Random.value;
            timer = enemy_intervale;
            GameObject xx;
            if (num <= 0.05)
                xx = Instantiate(Resources.Load("Enemy_9"), new Vector3(0, 0, 0), Quaternion.Euler(0, 0, 90)) as GameObject;
            else if (num <= 0.10)
                xx = Instantiate(Resources.Load("Enemy_8"), new Vector3(0, 0, 0), Quaternion.Euler(0, 0, 90)) as GameObject;
            else if (num <= 0.15)
                xx = Instantiate(Resources.Load("Enemy_7"), new Vector3(0, 0, 0), Quaternion.Euler(0, 0, 90)) as GameObject;
            else if (num <= 0.25)
                xx = Instantiate(Resources.Load("Enemy_6"), new Vector3(0, 0, 0), Quaternion.Euler(0, 0, 90)) as GameObject;
            else if (num <= 0.35)
                xx = Instantiate(Resources.Load("Enemy_5"), new Vector3(0, 0, 0), Quaternion.Euler(0, 0, 90)) as GameObject;
            else if (num <= 0.45)
                xx = Instantiate(Resources.Load("Enemy_4"), new Vector3(0, 0, 0), Quaternion.Euler(0, 0, 90)) as GameObject;
            else if (num <= 0.55)
                xx = Instantiate(Resources.Load("Enemy_3"), new Vector3(0, 0, 0), Quaternion.Euler(0, 0, 90)) as GameObject;
            else if (num <= 0.70)
                xx = Instantiate(Resources.Load("Enemy_2"), new Vector3(0, 0, 0), Quaternion.Euler(0, 0, 90)) as GameObject;
            else if (num <= 0.85)
                xx = Instantiate(Resources.Load("Enemy_1"), new Vector3(0, 0, 0), Quaternion.Euler(0, 0, 90)) as GameObject;
            else
                xx = Instantiate(Resources.Load("Enemy_1"), new Vector3(0, 0, 0), Quaternion.Euler(0, 0, 90)) as GameObject;
            siz.x = xx.GetComponent<SpriteRenderer>().bounds.size.x * xx.transform.localScale.x;
            siz.y = xx.GetComponent<SpriteRenderer>().bounds.size.y * xx.transform.localScale.y;
            Vector3 tmpPos = new Vector3(rightBottomCameraBorder.x - (siz.x / 2), Random.Range(
rightBottomCameraBorder.y + (siz.y / 2),
rightTopCameraBorder.y - (siz.y / 2)), 0);
            xx.transform.position = tmpPos;
        }
        else
        {
            if (!boss_spawn)
            {
                timer -= Time.deltaTime;
            }
            if (gameTime >= 0 && !boss_spawn)
            {
                gameTime -= Time.deltaTime;
            }
            else if (!boss_spawn)
            {
                GameObject boss = GameObject.FindGameObjectWithTag("Boss");
                boss.AddComponent<Boss_Spawn>();

                gameTime = loop_gametime;

                enemies_number += 10;
                enemy_intervale = gameTime / enemies_number;
                timer = enemy_intervale;
                
                boss_spawn = true;
            }
        }
    }
}
