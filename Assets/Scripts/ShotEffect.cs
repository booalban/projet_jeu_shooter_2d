using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotEffect : MonoBehaviour
{
    [SerializeField] private int damage;
    [SerializeField] private float seconds;
    [SerializeField] private float speed;

    private float timer = 0.6f;
    private Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Rigidbody2D>().velocity = transform.right * speed;
        animator = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        if (animator.GetBool("Trigger"))
        {
            if (timer <= 0)
                Destroy(gameObject);
            timer -= Time.deltaTime;
        }

        else if (seconds <= 0)
        {
            animator.SetBool("Trigger", true);
            GetComponent<BoxCollider2D>().enabled = false;
            GetComponent<Rigidbody2D>().velocity = transform.right * 0;
        }
        seconds -= Time.deltaTime;


    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Projectile" || collision.gameObject.tag == "Enemy" || collision.gameObject.tag == "Boss")
        {
            GetComponent<BoxCollider2D>().enabled = false;
            animator.SetBool("Trigger", true);
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            collision.gameObject.GetComponent<Enemies_Hp>().hp -= damage;
        }
    }
}
