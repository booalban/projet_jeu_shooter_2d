using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using TMPro;

public class Save : MonoBehaviour
{
    [SerializeField] private AudioMixer mixer;
    [SerializeField] private string effect_group_name;
    [SerializeField] private string music_group_name;
    [SerializeField] private TextMeshProUGUI high_score_1;
    [SerializeField] private TextMeshProUGUI high_score_2;
    [SerializeField] private TextMeshProUGUI high_score_3;
    [SerializeField] private TextMeshProUGUI high_score_infinite;

    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.HasKey(effect_group_name) && PlayerPrefs.HasKey(music_group_name))
        {
            mixer.SetFloat(effect_group_name, Mathf.Log10(PlayerPrefs.GetFloat(effect_group_name)) * 20);
            mixer.SetFloat(music_group_name, Mathf.Log10(PlayerPrefs.GetFloat(music_group_name)) * 20);
        }
        else
        {
            PlayerPrefs.SetFloat(effect_group_name, 1.0f);
            PlayerPrefs.SetFloat(music_group_name, 1.0f);
        }

        if(PlayerPrefs.HasKey("Level_1") && PlayerPrefs.HasKey("Level_2") && PlayerPrefs.HasKey("Level_3") && PlayerPrefs.HasKey("Level_infinite"))
        {
            high_score_1.text = PlayerPrefs.GetInt("Level_1").ToString();
            high_score_2.text = PlayerPrefs.GetInt("Level_2").ToString();
            high_score_3.text = PlayerPrefs.GetInt("Level_3").ToString();
            high_score_infinite.text = PlayerPrefs.GetInt("Level_infinite").ToString();
        }
        else
        {
            PlayerPrefs.SetInt("Level_1", 0);
            PlayerPrefs.SetInt("Level_2", 0);
            PlayerPrefs.SetInt("Level_3", 0);
            PlayerPrefs.SetInt("Level_infinite", 0);
        }

        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
