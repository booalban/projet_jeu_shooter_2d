using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Movement : MonoBehaviour
{
    [SerializeField] private Vector2 acceleration;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var joystick = Gamepad.current;
        Vector2 j_movement = joystick.leftStick.ReadValue();
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2 (j_movement.x * acceleration.x, j_movement.y * acceleration.y);
        
    }
}
