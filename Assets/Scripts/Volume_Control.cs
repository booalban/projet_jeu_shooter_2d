using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using TMPro;

public class Volume_Control : MonoBehaviour
{
    [SerializeField] private Slider slider;
    [SerializeField] private TextMeshProUGUI slider_value;
    [SerializeField] private AudioMixer mixer;

    void Start()
    {
        Load_volume();
    }

        public void Volume(float volume)
    {
        slider_value.text = volume.ToString("0.000");
        mixer.SetFloat(slider.name, Mathf.Log10(volume) * 20);
    }

    public void Save_volume()
    {
        PlayerPrefs.SetFloat(slider.name, slider.value);
        Load_volume();

    }

    public void Load_volume()
    {
        float volume = PlayerPrefs.GetFloat(slider.name);
        slider.value = volume;
        slider_value.text = volume.ToString("0.000");
        mixer.SetFloat(slider.name, Mathf.Log10(volume) * 20);
    }

}
