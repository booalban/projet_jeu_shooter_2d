using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimitCamera : MonoBehaviour
{

    private Vector3 upLeftCamera;
    private Vector3 bottomRightCamera;
    private Vector2 siz;

    // Start is called before the first frame update
    void Start()
    {
        upLeftCamera = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
        bottomRightCamera = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));


    }

    // Update is called once per frame
    void Update()
    {
        siz.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        siz.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;

        if(transform.position.y > upLeftCamera.y - (siz.y / 2))
        {
            transform.position = new Vector3(transform.position.x,
                                             upLeftCamera.y - (siz.y / 2),
                                             transform.position.z);
        }

        if (transform.position.y < bottomRightCamera.y + (siz.y / 2))
        {
            transform.position = new Vector3(transform.position.x,
                                             bottomRightCamera.y + (siz.y / 2),
                                             transform.position.z);
        }

        if (transform.position.x < upLeftCamera.x + (siz.x / 2))
        {
            transform.position = new Vector3(upLeftCamera.x + (siz.x /2),
                                             transform.position.y,
                                             transform.position.z);
        }

        if (transform.position.x > bottomRightCamera.x - (siz.x / 2))
        {
            transform.position = new Vector3(bottomRightCamera.x - (siz.x / 2),
                                             transform.position.y,
                                             transform.position.z);
        }

    }
}
