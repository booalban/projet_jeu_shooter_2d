using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesShotEffect : MonoBehaviour
{
    public float seconds;
    [SerializeField] private float speed;
    [SerializeField] private string type;

    private GameObject player;
    private float old_position_y_vaisseau;

    private float y;

    private float timer = 0.6f;
    private Animator animator;
    private GameObject health_bar;

    // Start is called before the first frame update
    void Start()
    {
        if(type == "Double")
        {
            timer = 1f;
        }
        if(type == "Follow")
        {
            timer = 0.5f;
        }
        else if(type == "Salve")
        {
            timer = 1.5f;
            y = Random.Range(-2.5f, 2.5f);

            Quaternion target = Quaternion.Euler(0, 0, 180 - (y * 4));

            transform.rotation = Quaternion.Slerp(transform.rotation, target, 1);
        }

        GetComponent<Rigidbody2D>().velocity = transform.right * speed;
        player = GameObject.FindGameObjectWithTag("Player");
        animator = GetComponent<Animator>();
        health_bar = GameObject.FindGameObjectWithTag("Health_bar");


    }

    // Update is called once per frame
    void Update()
    {
        if (!Pause_Menu.Paused)
        {
            if (animator.GetBool("Trigger"))
            {
                if (timer <= 0)
                {
                    Destroy(gameObject);
                }
                timer -= Time.deltaTime;
            }

            else if (seconds <= 0)
            {
                animator.SetBool("Trigger", true);
                GetComponent<Rigidbody2D>().velocity = transform.right * 0;
            }

            else if (type == "Follow" && player != null)
            {
                float x = player.transform.position.x;
                float y = player.transform.position.y;
                var pos = transform.position;
                if (x < pos.x)
                {

                    if ((pos.x - x) < 50 && (pos.y - y) < 50)
                    {
                        //pos.x -= (pos.x - x) / 150;
                        pos.y -= (pos.y - y) / 100;
                        old_position_y_vaisseau = y;
                    }
                    else if ((pos.x - x) < 25 && (pos.y - y) < 25)
                    {
                        //pos.x -= (pos.x - x) / 200;
                        pos.y -= (pos.y - y) / 150;
                    }
                    else
                    {
                        pos.y -= (pos.y - y) / 200;

                    }
                    transform.position = pos;
                }
                else
                {
                    pos.y -= (pos.y - old_position_y_vaisseau) / 150;
                    transform.position = pos;

                }
                Quaternion target = Quaternion.Euler(0, 0, -(pos.y * 10));

                transform.rotation = Quaternion.Slerp(transform.rotation, target, 1);

            }
            else if (type == "Double")
            {
                float range = Random.Range(0f, 1000f);

                if (range < 2)
                {

                    Vector3 pos1 = new Vector3(gameObject.transform.position.x - gameObject.GetComponent<SpriteRenderer>().bounds.size.x,
                       gameObject.transform.position.y + gameObject.GetComponent<SpriteRenderer>().bounds.size.y,
                       gameObject.transform.position.z);
                    Vector3 pos2 = new Vector3(gameObject.transform.position.x - gameObject.GetComponent<SpriteRenderer>().bounds.size.x,
                            gameObject.transform.position.y - gameObject.GetComponent<SpriteRenderer>().bounds.size.y,
                            gameObject.transform.position.z);
                    Quaternion rotation = Quaternion.Euler(0, 0, 0);
                    Quaternion rotation2 = Quaternion.Euler(0, 0, 0);

                    Instantiate(Resources.Load("ShotSplit1"), pos1, rotation);
                    Instantiate(Resources.Load("ShotSplit2"), pos2, rotation2);

                    Destroy(gameObject);
                }
                else
                {
                    timer -= Time.deltaTime;
                }
            }
            else if (type == "Diag_haut")
            {
                transform.position += new Vector3(0, -0.003f, 0);
            }
            else if (type == "Diag_bas")
            {
                transform.position += new Vector3(0, 0.003f, 0);
            }

            seconds -= Time.deltaTime;


        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
            animator.SetBool("Trigger", true);
            Vector2 health_lvl = health_bar.GetComponent<RectTransform>().sizeDelta;
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            if (health_lvl.x > 0)
            {
                if (type == "Default")
                {
                    health_bar.GetComponent<RectTransform>().sizeDelta = new Vector2(health_lvl.x - 86, health_lvl.y);
                }
                else if (type == "Elec")
                {
                    health_bar.GetComponent<RectTransform>().sizeDelta = new Vector2(health_lvl.x - 43, health_lvl.y);
                    collision.gameObject.AddComponent<Elec_Effect>();
                }
                else if (type == "Nova")
                {
                    health_bar.GetComponent<RectTransform>().sizeDelta = new Vector2(health_lvl.x - 215, health_lvl.y);
                }
                else if (type == "Laser" || type == "Follow" || type == "Salve")
                {
                    health_bar.GetComponent<RectTransform>().sizeDelta = new Vector2(health_lvl.x - 43, health_lvl.y);
                }
                else if (type == "Double")
                {
                    health_bar.GetComponent<RectTransform>().sizeDelta = new Vector2(health_lvl.x - 86, health_lvl.y);
                }
                else if (type == "Diag_haut" || type == "Diag_bas")
                {
                    health_bar.GetComponent<RectTransform>().sizeDelta = new Vector2(health_lvl.x - 86, health_lvl.y);
                }
            }
        }
        if (collision.gameObject.tag == "Player_Projectile" && type == "Double")
        {
            Vector3 pos1 = new Vector3(gameObject.transform.position.x - gameObject.GetComponent<SpriteRenderer>().bounds.size.x,
gameObject.transform.position.y + gameObject.GetComponent<SpriteRenderer>().bounds.size.y,
gameObject.transform.position.z);
            Vector3 pos2 = new Vector3(gameObject.transform.position.x - gameObject.GetComponent<SpriteRenderer>().bounds.size.x,
                    gameObject.transform.position.y - gameObject.GetComponent<SpriteRenderer>().bounds.size.y,
                    gameObject.transform.position.z);
            Quaternion rotation = Quaternion.Euler(0, 0, 0);
            Instantiate(Resources.Load("ShotTest"), pos1, rotation);
            Instantiate(Resources.Load("ShotTest"), pos2, rotation);
        }
    }
}
