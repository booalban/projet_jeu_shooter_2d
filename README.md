# ROGER'S SHOOTER
### projet réalisé sur unity 2020.3.25f1


## 1. DÉMARCHE EFFECTUÉE

Les premières séances de travail ont été dédiées à la prise d'informations sur les différents outils de développement.
Avant de nous lancer, nous nous sommes questionnés sur les différents éléments que nous aurions certainement besoin.
Nous avons agrémenté nos connaissances avec des notions comme la création et la gestion des animations. Ainsi que
L'ajout de sons, la gestion des menus et des différentes scènes. Durant cette phase, nous avons
également planifié l'aspect global de notre jeu en créant sur papier des maquettes pour organiser les niveaux, la
quantité d'ennemis par niveaux, les éventuels boss et les aspects visuels généraux.
En second lieu, nous avons commencé la phase de développement en commençant par s'attarder sur la recherche d'assets disponibles
sur l'asset store de Unity et d'autres site comme itch.io. Nous avons pu récupérer des assets sur les vaisseaux et adapter ou créer les animations.
Nous nous sommes ensuite attardés sur les scripts en C# à créer pour réaliser les différentes actions de notre jeu.
Les scripts créés permettent donc :

- la gestion des menus
- le passage de niveaux
- la gestion globale des niveaux (temps par niveau, nombre d'ennemis, apparition des boss)
- les scripts de spawn des ennemis
- les scripts associés aux caractéristiques des ennemis et du joueur (vie, déplacements, tirs)

Nous avons ajouté de la musique et du son pour les tirs et la destruction de vaisseaux.
Nous avons aussi ajouté des animations pour les apparitions des boss et les différents tirs.
Finalement, nous avons terminé notre développement par l'ajout d'un niveau infini permettant aux joueurs les plus aguerris.
de se défier en tentant de faire le meilleur score.


## 2. PRESENTATION GENERAL DU JEU

Notre jeu se présente comme un space shooter dans lequel l'objectif sera de finir les différents niveaux du jeu avec un score maximal.
Pour ce faire, il faut terminer les différents niveaux sans mourir et en éliminant le plus de vaisseaux ennemis.

Au lancement du jeu nous avons un menu principal avec trois options :
- la sélection d'un niveau de jeu
- la modification des sounds effects
- la sortie de l'application (quitte simplement le jeu)

Le menu des sounds effects contient deux éléments :
- la gestion de la musique
- la gestion des sons des tirs et des explosions

Le menu des niveaux permet de choisir son niveau. Les scores affichés sont les meilleurs scores des parties précédentes effectuées qui sont sauvegardées
automatiquement dans l'application. Lors du lancement de notre niveau, nous apparaissons avec une animation dans la zone ennemie dans laquelle
il faudra survivre aux vagues.
On peut de déplacer et nous disposons de deux modes de shoot: le classique ainsi que le tir explosif. Chaque tir consomme de la stamina qui
correspond à la barre bleue (le tir spécial consomme plus de stamina et occasionne également plus de dégât).
Les ennemis classiques spawn quant à eux de manière aléatoire. Les boss n'ont pas de déplacements aléatoires, mais ont des tirs spécifique.

On peut tirer sur les ennemis ainsi que sur les tirs ennemis pour les détruire.
Chaque élimination d'un ennemis participe à l'augmentation du score.


## 3. DIFFICULTÉS RENCONTRÉES

Nous avons rencontré plusieurs difficultés lors de la réalisation du projet, notamment avec les déplacements dans les animations.
Le "root motion" faisait déplacer l'objet sur le mauvais axe ou à une distance beaucoup trop élevée.
Par conséquent, nous avons remplacé l'animation par un script de déplacement que nous ajoutons à l'objet "Boss" une fois
que le timer du script GameCreator s'est écoulé.

Nous avons également tenté de réduire le nombre de "batches" du jeu en regroupant toutes les textures utilisées dans un fichier PNG.
Cependant, après avoir modifié certaines textures, nous nous sommes rendu compte que cela n'avait pas d'impact sur le nombre de "batches".
Par conséquent, nous avons décidé de revenir en arrière (le fichier PNG regroupant toutes les textures a été déplacé dans le dossier
textures fusionner pour prouver que cela a bien été réalisé).


## 4. TEST DE L'APPLICATION

Les dimensions ainsi que les positionnements sont fait en fonction des dimensions de l'appareil Android utilisé. Nous nous sommes assurés 
de cela en testant notre jeu sur différents appareils à commencé par les smartphones en particulier des téléphones avec des dimensions différentes, 
puis sur des tablettes :

- REDME NOTE 9 PRO (2400 x 1080 px)
- SAMSUNG GALAXY S8 (2220 x 1080 px)
- SAMSUNG GALAXY S4 mini
- SAMSUNG GALAXY S7
- SAMSUNG GALAXY TAB A 2016

Dans toutes les situations notre jeu arrivait à s'adapter aux différentes dimensions


## 5. RÉPARTITION DES TÂCHES

Alban MOREAU

- Création des menus du jeu
- Création des boss
- Gestion des niveaux (gestion des points de vie et de la stamina, de l'apparition des ennemis, de leur tir)
- Animations des tirs/des ennemis
- Level Design
- Choix des assets
- Choix des sons
- Gestion des bugs
- Phase de test sur différents mobiles


Pierre LIMA

- Création des déplacements des ennemis
- Création des boss
- Animations des tirs
- Choix des assets
- Gestion des bugs des déplacements
- Phase de tests sur différents mobiles





## 6. ASSETS UTILISÉS

- ultimate space game mega asset package par GameSupplyGuy https://gamesupply.itch.io/ultimate-space-game-mega-asset-package (musique et vaisseaux enemies)
- Free Enemy Spaceship 2D Sprites Pixel Art par Free Game Assets https://free-game-assets.itch.io/free-enemy-spaceship-2d-sprites-pixel-art (tirs et vaisseau du joueur)
- Sci-fi GUI skin par 3d.rina https://assetstore.unity.com/packages/2d/gui/sci-fi-gui-skin-15606 (Interface graphique)
- Warped Shooting Fx par Ansimuz https://assetstore.unity.com/packages/2d/textures-materials/abstract/warped-shooting-fx-195246 (tirs de boss)
- Space Background Generator par Deep-Fold https://deep-fold.itch.io/space-background-generator (Images de fond)

## 7. TÉLÉCHARGEMENT

- https://mega.nz/file/MEBxmLAY#Uch1Z6hxQrrUrNqDhGUdDDjbW9-Dgf9KAkSWLGLqzRY











